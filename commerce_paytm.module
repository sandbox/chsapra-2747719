<?php

/**
 * 
 * Implements Hook_menu
 */
function commerce_paytm_menu() {
  $items = array();
  $items['paytm-response'] = array(
    'title' => 'Thank You Page',
    'page callback' => 'paytm_response',
    'access arguments' => array('access content'),
  );

  $items['admin/config/development/paytm_payment_settings'] = array(
    'title' => 'Paytm Payment Settings Form',
    'description' => 'Paytm Payment Settings Form',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('paytm_payment_form'),
    'access arguments' => array('access paytm payment settings form'),
  );

  return $items;
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_paytm_commerce_payment_method_info() {
  $payment_methods = array();
  $display_title = t('Paytm Payment Gateway');
  $payment_methods['paytm_payment'] = array(
    'base' => 'paytm_payment',
    'title' => t("PAYTM payment"),
    'display_title' => $display_title,
    'short_title' => 'Commerce Paytm Payment Gateway',
    'description' => t("PayTM Payment Gateway"),
    'terminal' => TRUE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: settings form.
 */
function paytm_payment_settings_form($settings = NULL) {
  global $base_url;

  $form = array();
  $settings = (array) $settings + array(
    'merchant_key' => '',
    'mid' => '',
    'industry_type_id' => '',
    'channel_id' => '',
    'website' => '',
    'redirect_url' => '',
    'transaction_status_url' => '',
    'refund_url' => '',
    'payment_mode' => '',
  );


  $form['merchant_key'] = array(
    '#title' => t('Paytm Merchant Key'),
    '#type' => 'textfield',
    '#description' => t('Please enter the paytm merchant key'),
    '#default_value' => $settings['merchant_key'],
  );


  $form['mid'] = array(
    '#title' => 'Merchant Id',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $settings['mid'],
  );


  $form['industry_type_id'] = array(
    '#title' => 'Industry Type Id',
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => $settings['industry_type_id'],
  );


  $form['payment_mode'] = array(
    '#type' => 'select',
    '#title' => t('Payment mode'),
    '#description' => t('The mode for capturing payment.'),
    '#options' => array(
      'PROD' => t('Live transactions'),
      'TEST' => t('Test transactions'),
    ),
    '#default_value' => $settings['payment_mode'],
  );

  return $form;
}

/*
 * paytm settings form
 *
 */

function paytm_payment_form() {
  global $base_url;
  $form['paytm_merchant_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Paytm Merchant Key'),
    '#description' => t('Please enter the paytm merchant key'),
    '#default_value' => variable_get('paytm_merchant_key', ''),
  );
  $form['paytm_merchant_website'] = array(
    '#type' => 'textfield',
    '#title' => t('Paytm Merchant Website'),
    '#description' => t('Please enter the paytm merchant website'),
    '#default_value' => variable_get('paytm_merchant_website', ''),
  );
  $form['paytm_merchant_channel'] = array(
    '#type' => 'textfield',
    '#title' => t('Paytm Merchant channel'),
    '#description' => t('Please enter the paytm merchant channel'),
    '#default_value' => variable_get('paytm_merchant_channel', ''),
  );
  return system_settings_form($form);
}

/**
 * Payment method callback: redirect form.
 *
 * A wrapper around the module's general
 * use function for building a WPS form.
 */
function paytm_payment_redirect_form($form, &$form_state, $order, $payment_method) {
  global $user;
  global $base_url;
  $uid = $user->uid;
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $amount = $wrapper->commerce_order_total->amount->value() / 100;
  $mid = $payment_method['settings']['mid'];
  $industry_id = $payment_method['settings']['industry_type_id'];
  $website = variable_get('paytm_merchant_website');
  $merchant_key = variable_get('paytm_merchant_key');
  $channel_id = variable_get('paytm_merchant_channel');
  $payment_mode = $payment_method['settings']['payment_mode'];
  if ($payment_mode == 'TEST') {
    $redirect_url = 'https://pguat.paytm.com/oltp-web/processTransaction';
  }
  else {
    $redirect_url = 'https://secure.paytm.in/oltp-web/processTransaction';
  }

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $uid,
  );

  $form['merchant_key'] = array(
    '#type' => 'hidden',
    '#value' => $merchant_key,
  );

  $form['channel_id'] = array(
    '#type' => 'hidden',
    '#value' => $channel_id,
  );

  $form['mid'] = array(
    '#type' => 'hidden',
    '#value' => $mid,
  );

  $form['reference_no'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );

  $form['amount'] = array(
    '#type' => 'hidden',
    '#value' => $amount,
  );

  $form['redirect_url'] = array(
    '#type' => 'hidden',
    '#value' => $redirect_url,
  );

  $form['industry_id'] = array(
    '#type' => 'hidden',
    '#value' => $industry_id,
  );

  $form['website'] = array(
    '#type' => 'hidden',
    '#value' => $website,
  );

  $form['payment_mode'] = array(
    '#type' => 'hidden',
    '#value' => $payment_mode,
  );


  $link = $base_url . '/' . drupal_get_path('module', 'commerce_paytm') . "/paytm_form.php" . "";
  $form['#action'] = $link;

  return $form;
}

/**
 * Payment method response.
 * 
 */
function paytm_response() {

  header("Pragma: no-cache");
  header("Cache-Control: no-cache");
  header("Expires: 0");
  $drupal_path = $_SERVER['DOCUMENT_ROOT'];

  require_once 'lib/encdec_paytm.php';

  $paytmchecksum = "";
  $paramlist = array();
  $isvalidchecksum = FALSE;
  $MERCHANT_KEY = variable_get('paytm_merchant_key', '');
  $paramlist = $_POST;
  $paytmchecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg
  $isvalidchecksum = verifychecksum_e($paramlist, $MERCHANT_KEY, $paytmchecksum); //will return TRUE or FALSE string.

  if ($isvalidchecksum === TRUE) {
    if ($_POST["STATUS"] == "TXN_SUCCESS") {
      $payment_mode = "PAYTM";
      $order = commerce_order_load($_POST['ORDERID']);
      $timestamp = strtotime($_POST['TXNDATE']);
      if ($_POST['TXNAMOUNT'] == $order->commerce_order_total[LANGUAGE_NONE][0]['amount'] / 100) {
        $query = db_insert('paytm_payment_track');
        $query->fields(array(
          'order_id' => check_plain($_POST['ORDERID']),
          'amount' => check_plain($_POST['TXNAMOUNT']),
          'Result' => check_plain($_POST['RESPMSG']),
          'Transaction_Id' => check_plain($_POST['TXNID']),
          'Timestamp' => $timestamp,
          'Tamper_Flag' => '0',
          'Payment_Id' => $_POST['BANKTXNID'],
          'Response' => serialize($_POST),
          'Payment_mode' => $payment_mode,
        ));
        $result = $query->execute();
        $order->field_order_placement_time[LANGUAGE_NONE][0]['value'] = time();
        commerce_order_save($order);
        commerce_order_status_update($order, 'checkout_complete', $skip_save = FALSE, $revision = NULL, $log = '');
        $payment_method = $order->data['payment_method'];
        payment_transaction($payment_method, $order, check_plain($_POST['TXNAMOUNT']), check_plain($_POST['TXNID']), $_POST['BANKTXNID']);
        header('Location:' . $redirection_url . '/checkout/' . $_POST['ORDERID'] . '/complete');
      }
      else {
        $query = db_insert('paytm_payment_track');
        $query->fields(array(
          'order_id' => check_plain($_POST['ORDERID']),
          'amount' => check_plain($_POST['TXNAMOUNT']),
          'Result' => check_plain($_POST['RESPMSG']),
          'Transaction_Id' => '0',
          'Timestamp' => time(),
          'Tamper_Flag' => '0',
          'Payment_Id' => '0',
          'Response' => serialize($_POST),
          'Payment_mode' => $payment_mode,
        ));
        header('Location:' . $redirection_url . '/unsuccessfull_transaction');
      }
    }
    elseif ($_POST["STATUS"] == "TXN_FAILURE") {
      drupal_set_message(t("Payment was declined, Please try again!"), 'error');
      $order = commerce_order_load($_POST['ORDERID']);
      commerce_order_status_update($order, 'checkout_checkout', $skip_save = FALSE, $revision = NULL, $log = '');
      header('Location:' . $redirection_url . '/checkout/' . $_POST['ORDERID'] . '?repay=' . md5($_POST['ORDERID']));
    }
  }
  else {
    $query = db_insert('paytm_payment_track');
    $query->fields(array(
      'order_id' => check_plain($_POST['ORDERID']),
      'amount' => check_plain($_POST['TXNAMOUNT']),
      'Result' => check_plain($_POST['RESPMSG']),
      'Transaction_Id' => '0',
      'Timestamp' => time(),
      'Tamper_Flag' => '0',
      'Payment_Id' => '0',
      'Response' => serialize($_POST),
      'Payment_mode' => $payment_mode,
    ));
    header('Location:' . $redirection_url . '/unsuccessfull_transaction');
  }
}

/**
 * 
 * Creating a payment line item
 * @param unknown $payment_method
 * @param unknown $order
 * @param unknown $amount
 * @param number $transaction_id
 * @param unknown $payment_id
 * @return SAVED_NEW
 */
function payment_transaction($payment_method, $order, $amount, $transaction_id = 0, $payment_id) {
  $transaction = commerce_payment_transaction_new($payment_method, $order->order_id);
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $currency = $wrapper->commerce_order_total->currency_code->value();
  $transaction->amount = $amount * 100;
  $transaction->currency_code = 'INR';
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->message = t('Payment received at') . ' ' . date("d-m-Y H:i:s", REQUEST_TIME);
  $transaction->remote_status = t('Success');
  $transaction->remote_id = $payment_id;
  $transaction->payment_method = $payment_method['method'];
  $transaction->instance_id = $transaction_id;
  return commerce_payment_transaction_save($transaction);
}
