<?php
/*
 * Including Drupal Bootstrap
 */

$drupal_path = $_SERVER['DOCUMENT_ROOT'];

define('DRUPAL_ROOT', $drupal_path);

require DRUPAL_ROOT . '/includes/bootstrap.inc';
require DRUPAL_ROOT . '/sites/all/modules/custom/commerce_paytm/lib/encdec_paytm.php';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

$checkSum = "";
$paramList = array();

$ORDER_ID = $_POST['reference_no'];
$CUST_ID = $_POST['uid'];
$INDUSTRY_TYPE_ID = $_POST['industry_id'];
$CHANNEL_ID = $_POST['channel_id'];
$TXN_AMOUNT = $_POST['amount'];
$MID = $_POST['mid'];
$WEBSITE = $_POST['website'];
$MERCHANT_KEY = $_POST['merchant_key'];

// Create an array having all required parameters for creating checksum.
$paramList["MID"] = $MID;
$paramList["ORDER_ID"] = $ORDER_ID;
$paramList["CUST_ID"] = $CUST_ID;
$paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
$paramList["CHANNEL_ID"] = $CHANNEL_ID;
$paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
$paramList["WEBSITE"] = $WEBSITE;

$REDIRECTION_URL = $_POST['redirect_url'];

$checkSum = getChecksumFromArray($paramList, $MERCHANT_KEY);
watchdog('payment_mode', $checkSum);
?>
<html>
    <head>
        <title>Secure Payment</title>

    </head>
    <body>
    <center><h1>Please do not refresh this page...</h1></center>
    <form method="post" action="<?php echo $REDIRECTION_URL; ?>" name="frmTransaction" id="frmTransaction">
        <input name="MID"  type="hidden" value="<?php echo check_plain($MID); ?>" /> <br>
        <input name="ORDER_ID"  type="hidden" value="<?php echo check_plain($ORDER_ID); ?>" /> <br>
        <input name="CUST_ID"  type="hidden" value="<?php echo check_plain($CUST_ID); ?>" /> <br>
        <input name="INDUSTRY_TYPE_ID"  type="hidden" value="<?php echo check_plain($INDUSTRY_TYPE_ID); ?>" /> <br>
        <input name="CHANNEL_ID"  type="hidden" value="<?php echo check_plain($CHANNEL_ID); ?>" /> <br>
        <input name="TXN_AMOUNT"  type="hidden" value="<?php echo check_plain($TXN_AMOUNT); ?>" /> <br>
        <input name="WEBSITE"  type="hidden" value="<?php echo check_plain($WEBSITE); ?>" /> <br>        
        <input type="hidden" name="CHECKSUMHASH" value= "<?php echo $checkSum ?>"> <br>

    </form>
</body>
</html>


<script language="javascript">
  window.onload = function () {
      document.forms['frmTransaction'].submit();
  }
</script>
